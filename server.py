import json
import requests
import apiai
import urllib
import time

CLIENT_ACCESS_TOKEN = '035b80302baf4fe39bc07790da8d04da'
TOKEN = "590232797:AAGs-PvfkgoXOc000a4oQIZleZEWTz-oyA4"
URL = "https://api.telegram.org/bot{}/".format(TOKEN)


def get_url(url):
    response = requests.get(url)
    content = response.content.decode("utf8")
    return content


def get_json_from_url(url):
    content = get_url(url)
    js = json.loads(content)
    return js


def get_updates(offset=None):
    url = URL + "getUpdates"
    if offset:
        url += "?offset={}".format(offset)
    js = get_json_from_url(url)
    return js


def get_last_update_id(updates):
    update_ids = []
    for update in updates["result"]:
        update_ids.append(int(update["update_id"]))
    return max(update_ids)


message = """

        Aquí tienes enlaces de interés:
            - https://learnosm.org/es/

        Twitter 🐦
            https://twitter.com/OsmBolivia   

        🎉 Concurso 🎉
            Diseño del Logo de OSMBolivia
            Envianos tu logo

        Premios 👍
            Set de recuerditos  Wiki, stickers, bolis, agenda, taza

        Por último, déjanos conocerte mejor :
            - ¿Cómo te llamas?
            - ¿Qué edad tienes?
            - ¿Cual es tu correo Electronico?
            - ¿De que ciudad eres?
            - ¿Tienes conocimientos de Openstreetmap?

        """


def echo_all(updates):
    ai = apiai.ApiAI(CLIENT_ACCESS_TOKEN)    
    print("Updates >>> ",updates)

    for update in updates["result"]:
        chat = update["message"]["chat"]["id"]

        try:
            user_g = update['message']['new_chat_member']['first_name']
            new_chat_members = update['message']['new_chat_member']
            if new_chat_members:
                print("usuario nuevo")      
                send_message("Hola {} Bienvenid@ a OSMBolivia🇧🇴  😀".format(user_g), chat)
                send_message(message, chat)

        except:
            print("Cambios ... ")



def get_last_chat_id_and_text(updates):
    num_updates = len(updates["result"])
    last_update = num_updates - 1
    text = updates["result"][last_update]["message"]["text"]
    chat_id = updates["result"][last_update]["message"]["chat"]["id"]
    return (text, chat_id)


def send_message(text, chat_id):
    text = urllib.parse.quote_plus(text)
    url = URL + "sendMessage?text={}&chat_id={}".format(text, chat_id)
    get_url(url)


def main():
    last_update_id = None
    while True:
        updates = get_updates(last_update_id)
        if len(updates["result"]) > 0:
            last_update_id = get_last_update_id(updates) + 1
            echo_all(updates)
        time.sleep(0.5)


if __name__ == '__main__':
    main()